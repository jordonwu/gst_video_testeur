#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->directory->setText(QDir::homePath());
    ui->videoFileName->setText("test");
    initGStreamerElementsRecorder();
    populateConfigFileExtCombo();
    populate_recSource();
}

MainWindow::~MainWindow() {
    delete ui;
}

//*****  I N I T I A L I S A T I O N  **************************************************************

void MainWindow::initGStreamerElementsRecorder() {
    sceneVideo = new QGraphicsScene(this);
    ui->viewRecorder->setScene(sceneVideo);
    widgetVideo = new QGst::Ui::GraphicsVideoWidget;
    surfaceVideo = new QGst::Ui::GraphicsVideoSurface(ui->viewRecorder);
    widgetVideo->setSurface(surfaceVideo);
    sceneVideo->addItem(widgetVideo);
    widgetVideo->show();
    updateRecorderElementsView();
}

void MainWindow::updateRecorderElementsView(const int &index) {
    if(pipelineRecorderViewer)
        pipelineRecorderViewer.clear();  //clear the pointer, destroying the pipeline
    /* Create/find the elements */
    QGst::BinPtr videoSrcBin = createVideoSrcBin(index);
    QGst::ElementPtr sink    = surfaceVideo->videoSink();
    /* Create the empty pipeline */
    pipelineRecorderViewer = QGst::Pipeline::create("recorder-viewer-pipeline");
    if (!pipelineRecorderViewer || !videoSrcBin || !sink) {
        qDebug() << "Not all elements could be created.";
        exit(-1); }
    /* Build the pipeline */
    pipelineRecorderViewer->add(videoSrcBin);
    pipelineRecorderViewer->add(sink);
    /* create pads plugs and link them */
    QGst::PadPtr videoSinkPad = sink->getStaticPad("sink");
    QGst::PadPtr videoSrcPad = videoSrcBin->getStaticPad("src");
    videoSrcPad->link(videoSinkPad);
    pipelineRecorderViewer->bus()->addSignalWatch(); // connect the bus
    QGlib::connect(pipelineRecorderViewer->bus(), "message",
                   this, &MainWindow::onBusRECviewMessage);
}

void MainWindow::updateRecorderElements(const int &index) {
    if(pipelineRecorder)
        pipelineRecorder.clear();  //clear the pointer, destroying the pipeline
    QGst::BinPtr audioSrcBin = createAudioSrcBin();
    QGst::BinPtr videoSrcBin = createVideoSrcBin();
    QGst::ElementPtr mux = QGst::ElementFactory::make("oggmux");
    QGst::ElementPtr sink = QGst::ElementFactory::make("filesink");
    if (!audioSrcBin || !videoSrcBin || !mux || !sink) {
        QMessageBox::critical(this, tr("Error"), tr("One or more elements could not be created. "
                              "Verify that you have all the necessary element plugins installed."));
        return; }
    sink->setProperty("location", videoFile
                      );
    pipelineRecorder = QGst::Pipeline::create();
    pipelineRecorder->add(audioSrcBin, videoSrcBin, mux, sink);
    //link elements
    QGst::PadPtr audioPad = mux->getRequestPad("audio_%u");
    QGst::PadPtr audioSrcPad = audioSrcBin->getStaticPad("src");
    audioSrcPad->link(audioPad);
    QGst::PadPtr videoPad = mux->getRequestPad("video_%u");
    QGst::PadPtr videoSrcPad = videoSrcBin->getStaticPad("src");
    videoSrcPad->link(videoPad);
    mux->link(sink);
    pipelineRecorder->bus()->addSignalWatch(); // connect the bus
    QGlib::connect(pipelineRecorder->bus(), "message",
                   this, &MainWindow::onBusRecordMessage);
}

void MainWindow::populateConfigFileExtCombo() {
    ui->extName->addItem("ogv");
}

void MainWindow::populate_recSource() {
    QGst::ElementFactoryPtr uvch264src = QGst::ElementFactory::find("uvch264src");
    QGst::ElementFactoryPtr ximagesrc = QGst::ElementFactory::find("ximagesrc");
    ui->recSOURCE->addItem("Screen");
    if (!ximagesrc)
        qDebug() << "we don't have uvch264src, disable the choice to use it";
    else
        ui->recSOURCE->addItem("Camera ximagesrc");
    if (!uvch264src)
        qDebug() << "we don't have ximagesrc, disable the choice to use it";
    else
        ui->recSOURCE->addItem("Camera uvc-h264");
}

//*****  A C T I O N S  ****************************************************************************

void MainWindow::limitToCampaignDir(QString dir) {
    QRegExp rgx(QDir::homePath());
    if (rgx.indexIn(dir) == -1)
        dp->setDirectory(QDir::homePath());
}

QGst::BinPtr MainWindow::createAudioSrcBin() {
    QGst::BinPtr audioBin;
    try {
        audioBin = QGst::Bin::fromDescription("autoaudiosrc name=\"audiosrc\" ! audioconvert ! "
                                              "audioresample ! audiorate ! speexenc ! queue");
    } catch (const QGlib::Error & error) {
        qCritical() << "Failed to create audio source bin:" << error;
        return QGst::BinPtr(); }
    QGst::ElementPtr src = audioBin->getElementByName("audiosrc");
    //autoaudiosrc creates the actual source in the READY state
    src->setState(QGst::StateReady);
    return audioBin;
}

QGst::BinPtr MainWindow::createVideoSrcBin(const int &index) {
    QGst::BinPtr videoBin;
    try {
        switch (index) {
        case 0: //screencast
            videoBin = QGst::Bin::fromDescription("ximagesrc name=\"videosrc\" ! "
                                                  "videoconvert ! theoraenc ! queue");
            videoBin->getElementByName("videosrc")->setProperty("screen-num", 0);
            break;
        case 1:  //camera
            videoBin = QGst::Bin::fromDescription("autovideosrc name=\"videosrc\" !"
                                                  "videoconvert ! theoraenc ! queue");
            videoBin->getElementByName("videosrc")->setProperty("camera",0);
            break;
        case 2:
            videoBin = QGst::Bin::fromDescription("uvch264src name=\"videosrc\" ! "
                                                  "videoconvert ! theoraenc ! queue");
            videoBin->getElementByName("videosrc")->setProperty("camera",0);
            break; }
    } catch (const QGlib::Error & error) {
        qDebug() << "Failed to create video source bin:" << error;
        return QGst::BinPtr(); }
    return videoBin;
}

void MainWindow::onBusRecordMessage(const QGst::MessagePtr & message) {
    switch (message->type()) {
    case QGst::MessageEos:
        stop();  //got end-of-stream - stop the pipeline
        break;
    case QGst::MessageError:
        if (pipelineRecorder)  //check if the pipeline exists
            stop();
        QMessageBox::critical(this, tr("Pipeline Recorder Error"),
                              message.staticCast<QGst::ErrorMessage>()->error().message());
        break;
    default:
        break; }
}

void MainWindow::onBusRECviewMessage(const QGst::MessagePtr & message) {
    switch (message->type()) {
    case QGst::MessageEos:
        break;
    case QGst::MessageError:
        QMessageBox::critical(this, tr("Pipeline Recorder VIEW Error"),
                              message.staticCast<QGst::ErrorMessage>()->error().message());
        break;
    default:
        break; }
}

void MainWindow::start() {
    pipelineRecorder->setState(QGst::StatePlaying); // go!
    ui->recRecord->setText(tr("Stop recording"));
}

void MainWindow::stop() {
    pipelineRecorder->setState(QGst::StateNull); //stop recording
    ui->recRecord->setText(tr("Start recording"));
    ui->recRecord->setChecked(false);
}

//*****  -- P L A Y E R -- [TAB]  ******************************************************************

void MainWindow::on_playRR_clicked() {

}

void MainWindow::on_playPLAY_clicked() {

}

void MainWindow::on_playPAUSE_toggled(bool checked) {

}

void MainWindow::on_playSTOP_clicked() {

}

void MainWindow::on_playAVR_clicked() {

}

//*****  -- R E C O R D E R --[Tab]  ***************************************************************

void MainWindow::on_recSOURCE_currentIndexChanged(int index) {
    updateRecorderElementsView(index);
    updateRecorderElements(index);
}

void MainWindow::on_recPAUSE_toggled(bool checked) {
    (checked)
        ? pipelineRecorder->setState(QGst::StatePaused)
        : pipelineRecorder->setState(QGst::StatePlaying);
}

void MainWindow::on_recSTOP_clicked() {
    stop();
}

void MainWindow::on_recRecord_toggled(bool checked) {
    /* send an end-of-stream event to flush metadata and cause an EosMessage deliver */
    if (pipelineRecorder)
        pipelineRecorder->sendEvent(QGst::EosEvent::create());
    else // else if pipeline doesn't exist - start a new one
        start();
}

//*****  -- O V E R L A Y -- [Tab]  ****************************************************************

void MainWindow::on_overlayEntries_clicked(const QModelIndex &index) {

}

void MainWindow::on_overlayADD_clicked() {

}

void MainWindow::on_overlayDEL_clicked() {

}

void MainWindow::on_overlayChooseColor_clicked() {

}

void MainWindow::on_overlayChooseFont_clicked() {

}

void MainWindow::on_pushButtonoverlayCOPY_clicked() {

}

void MainWindow::on_overlayEDIT_clicked() {

}

void MainWindow::on_overlayREC_clicked() {

}

//*****  -- C O N F I G U R A T I O N -- [Tab]  ****************************************************

void MainWindow::on_chooseDirectory_clicked() {
    dp = new QFileDialog(this,tr("Choisir un répertoire d'enregistrement des fichiers du projet"),
                         QDir::homePath());
    connect(dp, SIGNAL(directoryEntered(QString)), this, SLOT(limitToCampaignDir(QString)));
    dp->setOption(QFileDialog::ShowDirsOnly);
    dp->setFileMode(QFileDialog::Directory);
    int answer = dp->exec();
    if (answer == 1)
        ui->directory->setText(dp->directory().absolutePath());
}

void MainWindow::on_videoFileName_textChanged(const QString &arg1) {
    videoFile = ui->directory->text() + "/" + arg1 + "." + ui->extName->currentText();
}

void MainWindow::on_extName_currentIndexChanged(const QString &arg1) {
    videoFile = ui->directory->text() + "/" + ui->videoFileName->text() + "." + arg1;
}

void MainWindow::on_overlayFileName_textChanged(const QString &arg1) {
    overlayFile = ui->directory->text() + "/" + arg1 + ".xml";
}
