#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDir>
#include <QFileDialog>
#include <QDebug>

#include <QGlib/Connect>
#include <QGlib/Error>
#include <QGst/Pipeline>
#include <QGst/ElementFactory>
#include <QGst/Pad>
#include <QGst/Bus>
#include <QGst/Message>
#include <QGst/Query>
#include <QGst/ClockTime>
#include <QGst/Event>
#include <QGst/StreamVolume>
#include <QGst/Ui/VideoWidget>
#include <QGst/Ui/GraphicsVideoSurface>
#include <QGst/Ui/GraphicsVideoWidget>
#include <QGst/videooverlay.h>

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void directoryEntered(QString);

private slots:
    void on_playRR_clicked();
    void on_playPLAY_clicked();
    void on_playPAUSE_toggled(bool checked);
    void on_playSTOP_clicked();
    void on_playAVR_clicked();
    void on_recSOURCE_currentIndexChanged(int index);
    void on_recPAUSE_toggled(bool checked);
    void on_recSTOP_clicked();
    void on_recRecord_toggled(bool checked);
    void on_overlayEntries_clicked(const QModelIndex &index);
    void on_overlayADD_clicked();
    void on_overlayDEL_clicked();
    void on_overlayChooseColor_clicked();
    void on_overlayChooseFont_clicked();
    void on_pushButtonoverlayCOPY_clicked();
    void on_overlayEDIT_clicked();
    void on_overlayREC_clicked();
    void on_chooseDirectory_clicked();
    void on_videoFileName_textChanged(const QString &arg1);
    void on_extName_currentIndexChanged(const QString &arg1);
    void on_overlayFileName_textChanged(const QString &arg1);
    void limitToCampaignDir(QString dir);

private:
    Ui::MainWindow                  *ui;
    QGraphicsScene                  *sceneOverlay, *sceneVideo;
    QGst::Ui::GraphicsVideoWidget   *widgetVideo;
    QGst::Ui::GraphicsVideoSurface  *surfaceVideo;
    QGst::PipelinePtr                pipelinePlayer, pipelineRecorderViewer, pipelineRecorder;
    QGst::Bus                       *bus;
    QGst::Message                   *msg;
    QFileDialog                     *dp;
    QString                          videoFile, overlayFile;
    void         initGStreamerElementsRecorder();
    void         updateRecorderElementsView(const int &index = 0);
    void         updateRecorderElements(const int &index);
    void         populateConfigFileExtCombo();
    void         populate_recSource();
    QGst::BinPtr createAudioSrcBin();
    QGst::BinPtr createVideoSrcBin(const int &index = 0);
    void onBusRECviewMessage(const QGst::MessagePtr & message);
    void onBusRecordMessage(const QGst::MessagePtr & message);
    void start();
    void stop();
};

#endif // MAINWINDOW_H
