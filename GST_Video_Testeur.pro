#-------------------------------------------------
#
# Project created by QtCreator 2016-08-17T07:21:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GST_Video_Testeur
TEMPLATE = app
CONFIG += link_pkgconfig
PKGCONFIG +=  Qt5GLib-2.0 \
    Qt5GStreamer-1.0 \
    Qt5GStreamerUi-1.0 \
    Qt5GStreamerUtils-1.0


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
