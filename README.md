# README #
test case application for learn to use Qt5-Gstreamer

### What i'm trying to do ###

* read video from file
* read video from source
* record video from source (in the same time the video is viewing)
* render video with a dynamic overlay text
* take snapshot video on the fly (without stop recording/view)
* modify video saturation/hue/color on the fly (while read or recording)
* do all of this with Qt5 framework (last version of Qt5 with QWidget/C++ code, no QML) in the easy way as possible

### Where i start from ? ###

* first tutorial: https://people.freedesktop.org/~tsaunier/gstreamer-website/sdk-basic-tutorial-concepts.html
* gstreamer (v1.8.2) official manual: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/index.html
* API review: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/qt-gstreamer/html/classQGst_1_1Ui_1_1GraphicsVideoWidget.html
* qtgstreamer IRC channel on server freenode
